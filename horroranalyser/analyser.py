import math
import pandas as pd
from collections import defaultdict, Counter
from horroranalyser.create_db import read_and_store_clean_subs
from nltk.tokenize import sent_tokenize


TERM_COUNT_THRESHOLD = 3


def create_term_count_matrix(index: dict, inverted_index: defaultdict) -> pd.DataFrame:
    matrix_df = pd.DataFrame(0, index=inverted_index.keys(), columns=index.keys())
    for key, value in matrix_df.iteritems():
        term_count = Counter(index[key])
        term_count_series = pd.Series(term_count)
        matrix_df[key] = term_count_series
    # remove values with occurrences less than 3
    matrix_df = matrix_df[matrix_df[:] > TERM_COUNT_THRESHOLD]
    # drop empty rows and empty columns
    matrix_df.dropna(how="all", inplace=True)
    # replace NaN cells with zeros
    matrix_df.fillna(0, inplace=True)
    return matrix_df


# returns the index
def get_exclamations(sub_dir: str, movie_list_path: str) -> dict:
    corpus_df = read_and_store_clean_subs(sub_dir, movie_list_path)
    exclamation_dict = {}
    for index, row in corpus_df.iterrows():
        clean_sub = row["cleaned"]
        if type(clean_sub) is not str and math.isnan(clean_sub):
            # we don't have subtitles for this movie
            continue
        exclamations = replace_commas(clean_sub.lower())
        exclamations = tokenize(exclamations)
        exclamations = filter_intersection_exclamations(exclamations)
        exclamations = filter_3_word_phrases(exclamations)
        exclamation_dict[index] = exclamations
    return exclamation_dict


def get_inverted_index(index: dict) -> defaultdict:
    inverted_index = defaultdict(set)
    for movie_id in index:
        for exclamation in index[movie_id]:
            inverted_index[exclamation].add(movie_id)
    return inverted_index


def replace_commas(subtitle: str) -> str:
    return subtitle.replace(",", "!")


def tokenize(subtitle: str) -> list[str]:
    return sent_tokenize(subtitle)


def filter_intersection_exclamations(tokenized_sub: list) -> list[str]:
    return [phrase for phrase in tokenized_sub if phrase.endswith("!")]


def filter_3_word_phrases(intersections: list) -> list[str]:
    return [phrase for phrase in intersections if len(phrase.split()) < 4]


