import re
import os
import sys
from multiprocessing import cpu_count, Pool
from typing import Union

test_str = ""

PATTERNS = {
    "newlines": r"\n+",
    "srt": {
        "time_stamp": r"\d+\n\d{2}:\s?\d{2}:\s?\d{2}[,.]\d{3}\s-?->\s\d{2}:\s?\d{2}:\s?\d{2}[,.]\d{3}",
        "html": r"</?i>|</?b>|</?u>|</?font\s?\w*>",
        "line_pos": r"\{.+?\}",
        "notes": r"\[.+\]",
        "minus": r"^-\s?"
    },
    "sub": {
        "time_stamp": r"\{\d*?\}\{\d*?\}(\{\w:.+\})*",
        "formatting": r"(\{\w:.+\})+"
    }

}

newlines = re.compile(PATTERNS["newlines"], re.MULTILINE)


def process_subtitle(filename: str, input_path: str, output_path: str, suffix: str = ""):
    if filename == -1:
        return
    clean_subtitle = clean(os.path.join(input_path, filename))
    if clean_subtitle == -1:
        return
    path = os.path.join(output_path, filename + suffix)
    with open(path, mode="w") as subtitle_file:
        subtitle_file.write(clean_subtitle)


def clean_directory(input_path: str, output_path: str, suffix=".clean"):
    file_list = [file for file in os.listdir(input_path) if not os.path.isdir(os.path.join(input_path, file))]
    pool = Pool(10)
    pool.starmap_async(process_subtitle, [(filename, input_path, output_path, suffix) for filename in file_list]).get()
    pool.close()
    pool.join()


def clean(path: str) -> Union[str, int]:
    with open(path, mode="r", errors="replace") as subtitle_file:
        subtitle_str = subtitle_file.read()
        if subtitle_file.name.endswith(".srt"):
            return srt_clean(subtitle_str)
        elif subtitle_file.name.endswith(".sub"):
            return sub_clean(subtitle_str)
        else:
            print("Sorry we don't support this format yet.")
            return -1


def srt_clean(srt: str) -> str:
    srt_patterns = PATTERNS["srt"]
    # remove time stamps
    clean_srt = re.sub(srt_patterns["time_stamp"], "", srt)
    # remove html tags <i>...</i>
    clean_srt = re.sub(srt_patterns["html"], "", clean_srt)
    # remove notes like [moaning]
    clean_srt = re.sub(srt_patterns["notes"], "", clean_srt)
    # remove line position tags {..}
    clean_srt = re.sub(srt_patterns["line_pos"], "", clean_srt)
    # remove minus at line beginning e,g. (- bla bla...)
    clean_srt = re.sub(srt_patterns["minus"], "", clean_srt, flags=re.MULTILINE)
    # remove new lines
    clean_srt = re.sub(PATTERNS["newlines"], " ", clean_srt)
    return newlines.sub(" ", clean_srt)[1:-1]


def sub_clean(sub: str) -> str:
    sub_pattern = PATTERNS["sub"]
    # remove time stamps
    clean_sub = re.sub(sub_pattern["time_stamp"], "", sub)
    # remove formatting eg. {y:i}
    clean_sub = re.sub(sub_pattern["formatting"], "", clean_sub)
    # remove |
    clean_sub = clean_sub.replace("|", " ")
    # remove newlines
    clean_sub = re.sub(PATTERNS["newlines"], " ", clean_sub)
    return newlines.sub(" ", clean_sub)[:-1]


