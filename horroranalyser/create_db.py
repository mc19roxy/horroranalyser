import re
import os
import pandas as pd
from multiprocessing import Pool, cpu_count

NAME_PATTERN = r"^(?P<movie_id>\d+)_"


def read_and_store_clean_subs(sub_dir: str, movie_list_path: str, output_path: str = None) -> pd.DataFrame:
    movies_df = read_movie_list(movie_list_path)
    file_list = [file for file in os.listdir(sub_dir) if not os.path.isdir(os.path.join(sub_dir, file))]
    pool = Pool(cpu_count() - 1)
    clean_subs = pool.starmap_async(process_clean_sub, [(filename, sub_dir) for filename in file_list]).get()
    pool.close()
    pool.join()
    movie_ids = [get_movie_id(movie_id[0]) for movie_id in clean_subs]
    subs = [sub[1] for sub in clean_subs]
    merge_df = pd.DataFrame({"cleaned": subs}, index=movie_ids)
    merge_df = merge_df[~merge_df.index.duplicated()]
    res_df = pd.merge(movies_df, merge_df, left_index=True, right_index=True, how="left")
    if output_path is not None:
        res_df.to_csv(output_path, encoding="utf-8")
    return res_df


def process_clean_sub(filename: str, sub_path: str) -> tuple[str, str]:
    clean_sub = read_cleaned_file(filename, sub_path)
    # print(movies_df)
    return filename, clean_sub


def read_cleaned_file(filename: str, input_path: str) -> str:
    with open(os.path.join(input_path, filename), mode="r") as clean_sub_file:
        clean_sub = clean_sub_file.read()
    return clean_sub


def read_movie_list(path: str) -> pd.DataFrame:
    movie_list_df = pd.read_csv(path)
    movie_list_df.index += 2
    print(movie_list_df)
    return movie_list_df


def get_movie_id(filename: str) -> int:
    m = re.match(NAME_PATTERN, filename)
    movie_id = int(m.group("movie_id"))
    return movie_id
